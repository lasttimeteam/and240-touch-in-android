﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace XamTouch
{
    public class PaintView : View
    {
        
        Dictionary<int, Paint> paints = new Dictionary<int, Paint>();
        Dictionary<int, MotionEvent.PointerCoords> coords = new Dictionary<int, MotionEvent.PointerCoords>();

        private Canvas drawCanvas;
        private Bitmap canvasBitmap;

        public PaintView(Context context) : base(context)
        {
        }

        public PaintView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public PaintView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
        {
            base.OnSizeChanged(w, h, oldw, oldh);

            canvasBitmap = Bitmap.CreateBitmap(w, h, Bitmap.Config.Argb8888);

            drawCanvas = new Canvas(canvasBitmap);
        }

        public void Clear()
        {
            drawCanvas.DrawColor(Color.Black, PorterDuff.Mode.Clear);
            Invalidate();
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            switch (e.ActionMasked)
            {
                case MotionEventActions.Down: //First finger down, use 0 for the index
                {
                    int id = e.GetPointerId(0);

                    paints.Add(id, GenerateRandomColorPaint());

                    var start = new MotionEvent.PointerCoords();
                    e.GetPointerCoords(id, start);
                    coords.Add(id, start);

                    return true;
                }
                case MotionEventActions.PointerDown:
                {
                    int id = e.GetPointerId(e.ActionIndex);

                    paints.Add(id, GenerateRandomColorPaint());

                    var start = new MotionEvent.PointerCoords();
                    e.GetPointerCoords(id, start);
                    coords.Add(id, start);

                    return true;
                }
                case MotionEventActions.Move: // Move contains data for all current fingers
                {
                    for (int i = 0; i < e.PointerCount; i++)
                    {
                        var id = e.GetPointerId(i);

                        float x = e.GetX(i);
                        float y = e.GetY(i);

                        drawCanvas.DrawLine(coords[id].X, coords[id].Y, x, y, paints[id]);

                        coords[id].X = x;
                        coords[id].Y = y;
                    }

                    Invalidate();

                    return true;
                }

                case MotionEventActions.PointerUp: // Other finger up, use ActionIndex for the index
                {
                    int id = e.GetPointerId(e.ActionIndex);

                    paints.Remove(id);
                    coords.Remove(id);

                    return true;
                }

                case MotionEventActions.Up: // Last finger up, use 0 for the index
                {
                    int id = e.GetPointerId(0);

                    paints.Remove(id);
                    coords.Remove(id);

                    return true;
                }

                default:
                    return false;
            }
        }

        protected override void OnDraw(Canvas canvas)
        {
            canvas.DrawBitmap(canvasBitmap, 0,0, null);
        }

        /*
         * Helper method to generate randomly colored paint objects which will be used to render the Paths
         */
        private Paint GenerateRandomColorPaint()
        {
            var random = new Random((int)DateTime.Now.Ticks);

            var color = Color.Argb(255, random.Next(256), random.Next(256), random.Next(256));

            var paint = new Paint {Color = color, StrokeWidth = 5f, AntiAlias = true};
            paint.SetStyle(Paint.Style.Stroke);

            return paint;
        }
    }
}